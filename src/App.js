import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import DataTable from './component/data.table';

function App() {
  return (
    <div>
      <DataTable></DataTable>
    </div>
  );
}

export default App;
