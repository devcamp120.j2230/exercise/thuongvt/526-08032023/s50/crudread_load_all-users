import { Grid, Pagination, Typography } from "@mui/material";
import { Container } from "@mui/system";
import { useEffect, useState } from "react";
import { Button, Table } from "react-bootstrap";
import ModelAddUser from "./modelAddUser";
import ModelDeleteUser from "./modelDeleteUser";
import ModelPutUser from "./modelPutUser";



function DataTable() {

    const [number, setMunber] = useState(10)
    const [rows, setRows] = useState([]);
    const [totalPage, setTotalPage] = useState(0);
    const [curPage, setCurPage] = useState(1)
    const [openModelAdd, setOpenModelAdd] = useState(false);
    const [openModelPutUser, setOpenModelPutUser] = useState(false)
    const [openModelDelete, setOpenModelDelete] = useState(false)
    const [dataUserPut, setDataUserPut] = useState([])
    const [refreshPage, setRefreshPage] = useState(0)
    const [dataUserDelete, setDataUserDelete] = useState([])

    const fetchAPI = async (url) => {
        const res = await fetch(url);
        const data = res.json();
        console.log(data)
        return data;
    }

    const handleChangPage = (event, value) => {
        setCurPage(value)
    }

    const clickBtnFix = (row) => {
        setDataUserPut(row)
        setOpenModelPutUser(true)
        console.log(row)
    }

    const clickBtnDelete = (row) => {
        setDataUserDelete(row)
        setOpenModelDelete(true)
        console.log(row)
    }

    const clickBtnAddUser = () => {
        setOpenModelAdd(true)
    }

    const onClickCloseModalAdd = () => {
        setOpenModelAdd(false)
    }

    const onClickCloseModalPutUser = () => {
        setOpenModelPutUser(false)
    }
    const onClickCloseModalDeleteUser = ()=>{
        setOpenModelDelete(false)
    }
    useEffect(() => {
        fetchAPI("http://203.171.20.210:8080/crud-api/users/")
            .then((data) => {
                setTotalPage(Math.ceil(data.length / number))
                setRows(data.slice((curPage - 1) * number, curPage * number))
            })
            .catch((error) => {
                console.error(error.message)
            })
    }, [curPage, refreshPage])


    return (
        <Container>
            <Grid container mt={5}>
                <Grid item xs={12} sm={12} md={12} lg={12}>
                    <Typography style={{ textAlign: "center" }}><h3>Danh sách người dùng đăng ký</h3></Typography>
                </Grid>
                <Grid item xs={12} sm={12} md={12} lg={12}>
                    <Button style={{ width: "120px", height: "50px", marginBottom: "20px" }} onClick={clickBtnAddUser}>Thêm user</Button>
                </Grid>
                <Grid item xs={12} sm={12} md={12} lg={12}>
                    <Table striped bordered hover style={{ margin: "auto" }}>
                        <thead>
                            <tr>
                                <th style={{ padding: "20px" }}>iD</th>
                                <th style={{ padding: "20px" }}>First Name</th>
                                <th style={{ padding: "20px" }}>Last Name</th>
                                <th style={{ padding: "20px" }}>Country</th>
                                <th style={{ padding: "20px" }}>Subject</th>
                                <th style={{ padding: "20px" }}>customerType</th>
                                <th style={{ padding: "20px" }}>registerStatus</th>
                                <th style={{ padding: "20px" }}>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            {rows.map((row, id) => {
                                return (
                                    <tr key={id}>
                                        <td style={{ padding: "20px" }}>{row.id}</td>
                                        <td style={{ padding: "20px" }}>{row.firstname}</td>
                                        <td style={{ padding: "20px" }}>{row.lastname}</td>
                                        <td style={{ padding: "20px" }}>{row.country}</td>
                                        <td style={{ padding: "20px" }}>{row.subject}</td>
                                        <td style={{ padding: "20px" }}>{row.customerType}</td>
                                        <td style={{ padding: "20px" }}>{row.registerStatus}</td>
                                        <td style={{ padding: "20px" }}><Button class="btn btn-light" style={{ backgroundColor: "green", color: "white", margin: "5px" }} onClick={() => clickBtnFix(row)}>Sửa</Button><Button class="btn btn-light" style={{ color: "white" }} onClick={() => clickBtnDelete(row)}>Xóa</Button></td>
                                    </tr>
                                )
                            })}
                        </tbody>
                    </Table>
                </Grid>
                <Grid item>
                    <Pagination count={totalPage} defaultPage={curPage} onChange={handleChangPage}></Pagination>
                </Grid>
                <>
                    <ModelAddUser
                        openProp={openModelAdd}
                        closeProp={onClickCloseModalAdd}
                    ></ModelAddUser>
                </>
                <>
                    <ModelPutUser
                        openProp={openModelPutUser}
                        closeProp={onClickCloseModalPutUser}
                        dataProp={dataUserPut}
                        refreshPage={refreshPage}
                        setRefreshPage={setRefreshPage}

                    >
                    </ModelPutUser>
                </>
                <>
                    <ModelDeleteUser
                        openProp={openModelDelete}
                        closeProp={onClickCloseModalDeleteUser}
                        dataProp={dataUserDelete}
                        refreshPage={refreshPage}
                        setRefreshPage={setRefreshPage}
                    >
                    </ModelDeleteUser>
                </>
            </Grid>
        </Container>)
}

export default DataTable