import { Alert, Grid, Modal, Snackbar } from "@mui/material";
import Box from '@mui/material/Box';
import { useEffect, useState } from "react";
import { Button, Col, Container, Row } from "reactstrap";

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 700,
    bgcolor: "green",
    boxShadow: 24,
    p: 4,
};

function ModelPutUser({ openProp, closeProp, dataProp,refreshPage,setRefreshPage }) {



    const getData = async (paramUrl, paramOption = {}) => {
        const response = await fetch(paramUrl, paramOption);
        const data = await response.json();
        return data
    }


    const [alertForm, setAlertForm] = useState("");
    const [openAlert, setOpenAlert] = useState(false);
    const [id, setId] = useState("")
    const [lastname, setLastname] = useState("");
    const [firstname, setFirstname] = useState("");
    const [subject, setSubject] = useState("");
    const [country, setCountry] = useState("");
    const [registerStatus, setRegisterStatus] = useState("");
    const [customerType, setCustomerType] = useState("")

    const handleCloseAlert = () => {
        setOpenAlert(false)
    }
    const clickHandleCancelPut = () => {
        setOpenAlert(true)
        setAlertForm("Hủy bỏ sửa user")
        closeProp()
    }

    const clickHandleEditUser = () => {
        console.log("nút edit user đã được bấm")
        var checkData = (validateData());
        let abc = {
                   firstname: firstname,
                    lastname: lastname,
                    country: country,
                    customerType: customerType,
                    registerStatus: registerStatus,
                    subject: subject
        }
        console.log(abc)
        if (checkData) {

            const body = {
                method: 'PUT',
                body: JSON.stringify(abc),
                headers: {
                    'Content-Type': 'application/json',
                }
            }

            console.log(body)

            getData('http://203.171.20.210:8080/crud-api/users/' + id, body)
            .then((data)=>{
                console.log(data)
                setOpenAlert(true)
                setAlertForm("Hoàn thành sửa thông tin người dùng")
                setRefreshPage(refreshPage + 1);
                closeProp()
            })
            .catch((err) => {
                console.error(err.message)
                setOpenAlert(true)
                setAlertForm("Không sửa thông tin người dùng")
            })
        }


    }

    const validateData = (param) => {
        if (!lastname) {
            alert("lastName chưa có");
            return false
        }
        if (!firstname) {
            alert("firstname chưa có");
            return false
        }
        if (!country) {
            alert("country chưa có");
            return false
        }
        if (!subject) {
            alert("subject chưa có");
            return false
        }
        return true
    }



    useEffect(() => {
        setId(dataProp.id);
        setLastname(dataProp.lastname);
        setFirstname(dataProp.firstname);
        setCountry(dataProp.country);
        setCustomerType(dataProp.customerType);
        setSubject(dataProp.subject);
        setRegisterStatus(dataProp.registerStatus);
    }, [openProp])


    return (
        <Grid>
            <Modal
                open={openProp}
                onClose={closeProp}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box sx={style}>
                    <Col style={{ textAlign: "center", color: "white" }}><h3>Sửa thông tin khách hàng</h3></Col>
                    <Container>
                        <Row>
                            <div class="form-group">
                                <label for="exampleInputEmail1">id</label>
                                <input type="text" class="form-control" aria-describedby="emailHelp" placeholder="lastname" value={id} onChange={(e) => setId(e.target.value)} />
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Lastname</label>
                                <input type="text" class="form-control" aria-describedby="emailHelp" placeholder="lastname" value={lastname} onChange={(e) => setLastname(e.target.value)} />
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Firstname</label>
                                <input type="text" class="form-control" aria-describedby="emailHelp" placeholder="Firstname" value={firstname} onChange={(e) => setFirstname(e.target.value)} />
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Subject</label>
                                <input type="text" class="form-control" aria-describedby="emailHelp" placeholder="Subject" value={subject} onChange={(e) => setSubject(e.target.value)} />
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Country</label>
                                <select class="form-control" value={country} onChange={(e) => setCountry(e.target.value)}>
                                    <option selected>Chọn Country</option>
                                    <option value="VN">Việt Nam</option>
                                    <option value="USA">USA</option>
                                    <option value="AUS">Australia</option>
                                    <option value="CAN">Canada</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Customer Type</label>
                                <select class="form-control" value={customerType} onChange={(e) => setCustomerType(e.target.value)}>
                                    <option value="Standard">Standard</option>
                                    <option value="Gold">Gold</option>
                                    <option value="Premium">Premium</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">register</label>
                                <select class="form-control" value={registerStatus} onChange={(e) => setRegisterStatus(e.target.value)} >
                                    <option value="Accepted">Accepted</option>
                                    <option value="Denied">Denied</option>
                                    <option value="Standard">Standard</option>
                                </select>
                            </div>
                        </Row>
                        <Row style={{ marginTop: "10px", textAlign: "center" }}>
                            <Col><Button style={{ width: "100px", backgroundColor: "rebeccapurple" }} onClick={clickHandleEditUser} >Edit user</Button></Col>
                            <Col><Button style={{ width: "100px", backgroundColor: "red" }} onClick={clickHandleCancelPut}>Hủy Bỏ</Button></Col>
                        </Row>
                    </Container>
                </Box>
            </Modal>
            <Snackbar autoHideDuration={6000} open={openAlert} onClose={handleCloseAlert}>
                <Alert sx={{ width: '100%' }}>
                    {alertForm}
                </Alert>
            </Snackbar>
        </Grid>

    )
}

export default ModelPutUser