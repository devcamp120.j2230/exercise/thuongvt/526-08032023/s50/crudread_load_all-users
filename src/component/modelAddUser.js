import { Alert, Grid, Modal, Snackbar } from "@mui/material";
import Box from '@mui/material/Box';
import { useState } from "react";
import { Button, Col, Container, Row } from "reactstrap";


const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 700,
    bgcolor: "orange",
    boxShadow: 24,
    p: 4,
};

function ModelAddUser({ openProp, closeProp }) {


    const getData = async (paramUrl, paramOption = {}) => {
        const response = await fetch(paramUrl, paramOption);
        const data = await response.json();
        return data
    }


    const [lastname, setLastname] = useState("");
    const [firstname, setFirstname] = useState("");
    const [subject, setSubject] = useState("");
    const [country, setCountry] = useState("")
    const [alertForm, setAlertForm] = useState("");
    const [openAlert, setOpenAlert] = useState(false);



     const handleCloseAlert =()=>{
        setOpenAlert(false)
     }
    const handleChange = (event) => {
        setCountry(event.target.value)
    }
    // const handleChangeRegister = (event) => {
    //     setRegisterStatus(event.target.value)
    // }
    const clickHandleCancel = ()=>{
            setOpenAlert(true)
            setAlertForm("Hủy bỏ thêm mới user")
            closeProp()
    }

    const clickHandleInsertUser = () => {
        var DataAddUser = {
            lastname: lastname,
            firstname: firstname,
            country: country,
            subject: subject,
        };
        var checkData = (validateData());
            console.log(DataAddUser)
        if (checkData) {
            const body = {
                method: "POST",
                body: JSON.stringify(DataAddUser),
                headers: {
                    'Content-Type': 'application/json',
                }
            }

            getData('http://203.171.20.210:8080/crud-api/users/', body)
                .then((data) => {
                    console.log(data)
                    setOpenAlert(true)
                    setAlertForm("Thành công thêm dữ liệu")
                    closeProp()
                })
                .catch((err) => {
                    console.error(err.message)
                })

        }

    }

    const validateData = () => {
        if (!lastname) {
            alert("lastName chưa có");
            return false
        }
        if (!firstname) {
            alert("firstname chưa có");
            return false
        }
        if (!country) {
            alert("country chưa có");
            return false
        }
        if (!subject) {
            alert("subject chưa có");
            return false
        }
        return true
    }



    return (
        <Grid>
            <Modal
                open={openProp}
                onClose={closeProp}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box sx={style}>
                    <Col style={{ textAlign: "center" }}><h3>Thêm người dùng mới</h3></Col>
                    <Container>
                        <Row>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Lastname</label>
                                <input type="text" class="form-control" aria-describedby="emailHelp" placeholder="lastname" onChange={(e) => setLastname(e.target.value)} value={lastname} />
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Firstname</label>
                                <input type="text" class="form-control" aria-describedby="emailHelp" placeholder="Firstname" onChange={(e) => setFirstname(e.target.value)} value={firstname} />
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Subject</label>
                                <input type="text" class="form-control" aria-describedby="emailHelp" placeholder="Subject" onChange={(e) => setSubject(e.target.value)} value={subject} />
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Country</label>
                                <select class="form-control" value={country} onChange={handleChange}>
                                    <option selected>Chọn Country</option>
                                    <option value="VN">Việt Nam</option>
                                    <option value="USA">USA</option>
                                    <option value="AUS">Australia</option>
                                    <option value="CAN">Canada</option>
                                </select>
                            </div>
                            {/* <div class="form-group">
                                <label for="exampleInputEmail1">register</label>
                                <select class="form-control" value={registerStatusForm} onChange={handleChangeRegister}>
                                    <option value="Accepted">Accepted</option>
                                    <option value="Denied">Denied</option>
                                    <option value="Standard">Standard</option>
                                </select>
                            </div> */}
                        </Row>
                        <Row style={{ marginTop: "10px", textAlign: "center" }}>
                            <Col><Button style={{ width: "100px" }} onClick={clickHandleInsertUser}>insert user</Button></Col>
                            <Col><Button style={{ width: "100px" }} onClick={clickHandleCancel}>Hủy Bỏ</Button></Col>
                        </Row>
                    </Container>
                </Box>
            </Modal>
            <Snackbar open={openAlert} autoHideDuration={6000} onClose={handleCloseAlert}>
                <Alert  sx={{ width: '100%' }}>
                   {alertForm}
                </Alert>
            </Snackbar>

        </Grid>
    )
}
export default ModelAddUser