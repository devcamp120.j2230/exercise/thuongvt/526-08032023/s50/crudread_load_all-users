import { Alert, Grid, Modal, Snackbar } from "@mui/material";
import Box from '@mui/material/Box';
import { useEffect, useState } from "react";
import { Button, Col, Container, Row } from "reactstrap";

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: "blue",
    boxShadow: 24,
    p: 4,
};


function ModelDeleteUser({ openProp,closeProp,dataProp,refreshPage,setRefreshPage}) {

    

    const [openAlert, setOpenAlert] = useState(false);
    const [alertForm, setAlertForm] = useState("");
    const [id, setId] = useState("")




    const handleCloseAlert =()=>{
        setOpenAlert(false)
     }

     const clickHandleCancelDelete =()=>{
        setOpenAlert(true)
        setAlertForm("Hủy bỏ xóa user")
        closeProp()
     }

     const clickHandleDelete = ()=>{
        console.log("nút edit user đã được bấm")
        var idDelete = dataProp.id
        console.log(idDelete)

        var checkData = (validateData());

        if(checkData){
            const body = {
                method: 'DELETE'
            }
        // ko cần khai báo hàm getdata vì nó không trả về json 
            fetch('http://203.171.20.210:8080/crud-api/users/'+ idDelete,body)
            .then((data)=>{
                console.log(data)
                setOpenAlert(true)
                setAlertForm("Hoàn thành xóa thông tin người dùng")
                setRefreshPage(refreshPage + 1);
                closeProp()
            })
            .catch((err)=>{
                console.error(err.message)
                setOpenAlert(true)
                setAlertForm("Không xóa thông tin người dùng")
            })
        }
     }


     const validateData = ()=>{
        if(!id){
            alert("không có id")
            return false
        }
        return true
     }


     useEffect(() => {
        setId(dataProp.id);
    }, [openProp])

    return (
            <Grid>
                <Modal
                    open={openProp}
                    onClose={closeProp}
                    aria-labelledby="modal-modal-title"
                    aria-describedby="modal-modal-description"
                >
                    <Box sx={style}>
                        <Col style={{ textAlign: "center",color:"white" }}><h3>Xóa người dùng</h3></Col>
                        <Container>
                            <Row>
                                <div style={{ textAlign: "center",color:"white" }}>
                                    <p>Bạn có muốn xóa thông tin người dùng</p>
                                </div>
                            </Row>
                            <Row style={{ marginTop: "10px", textAlign: "center" }}>
                                <Col><Button style={{ width: "100px",backgroundColor:"yellowgreen" }}  onClick={clickHandleDelete} > Xóa User</Button></Col>
                                <Col><Button style={{ width: "100px",backgroundColor:"orange"}} onClick={clickHandleCancelDelete}>Hủy Bỏ</Button></Col>
                            </Row>
                        </Container>
                    </Box>
                </Modal>
                <Snackbar open={openAlert} autoHideDuration={6000} onClose={handleCloseAlert}>
                    <Alert sx={{ width: '100%' }}>
                        {alertForm}
                    </Alert>
                </Snackbar>
            </Grid>
    )
}

export default ModelDeleteUser





